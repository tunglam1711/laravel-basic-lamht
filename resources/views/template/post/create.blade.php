@extends('template.dashboard')
@section('title')
    <title>Create Post</title>
@endsection
@section('content')
    <div class="card">
        <h5 class="card-header">Create Post</h5>
        <div class="card-body">
            <form action="{{route('post.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label >Title</label>
                    <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{old('title')}}">
                    @error('title')
                    <div class="alert text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label >Description</label>
                    <textarea type="text" class="form-control @error('description') is-invalid @enderror" name="description">{{old('description')}}</textarea>
                    @error('description')
                    <div class="alert text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label >Content</label>
                    <textarea type="text" class="form-control @error('content') is-invalid @enderror" name="content">{{old('content')}}</textarea>
                    @error('content')
                    <div class="alert text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <input type="radio" name="status" value="0" class="mx-2" id="status0" class="status">
                    <label for="status0">Hide</label>
                    <input type="radio" name="status" value="1" class="mx-2" id="status1" class="status">
                    <label for="status1">Visible</label>
                    @error('status')
                    <div class="alert text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button class="btn btn-primary text-white" type="submit">Submit</button>
            </form>
        </div>
    </div>
@endsection
