@extends('template.dashboard')
@section('title')
    <title>View Post</title>
@endsection
@section('content')
    <div class="card">
        <div class="card-header d-flex align-items-center justify-content-between">
            <h4 class="d-block">View Post</h4>
            <a href="{{route('post.create')}}" class="btn btn-primary btn-sm text-white d-block">Add Post</a>

        </div>
        <div class="card-body">
            @if(session('message'))
                <span class="text-success">{{session('message')}}</span>
            @endif
            <table class="table table-bordered table-striped table-bordered table-hover">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Content</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($posts as $post)
                <tr>
                    <th scope="row">{{$post->id}}</th>
                    <td>{{$post->title}}</td>
                    <td>{{$post->description}}</td>
                    <td>{{$post->content}}</td>
                    <td>{{$post->status=='0'?"Hide":'Visible'}}</td>
                    <td>
                        <a href="{{route('post.edit',['id'=>$post->id])}}" class="btn btn-sm btn-warning text-white">Edit</a>
                        <a href="{{route('post.destroy',['id'=>$post->id])}}" class="btn btn-sm btn-danger text-white" onclick="return confirm('Are you sure?')">Delete</a>
                    </td>
                </tr>
                @empty
                    <tr >
                        <td colspan="6">No Post Found!!!!</td>
                    </tr>
                @endforelse
                </tbody>

            </table>
                {{$posts->links()}}
        </div>
    </div>

@endsection
