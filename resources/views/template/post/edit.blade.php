@extends('template.dashboard')
@section('title')
    <title>Edit Post</title>
@endsection
@section('content')
    <div class="card">
        <h5 class="card-header">Edit Post</h5>
        <div class="card-body">
            <form action="{{route('post.update',['id'=>$post->id])}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label >Title</label>
                    <input type="text" class="form-control" name="title" value="{{$post->title}}">
                </div>
                <div class="form-group">
                    <label >Description</label>
                    <textarea type="text" class="form-control" name="description">{{$post->description}}</textarea>
                </div>
                <div class="form-group">
                    <label >Content</label>
                    <textarea type="text" class="form-control" name="content">{{$post->content}}</textarea>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <input type="radio" name="status" value="0" class="mx-2" id="status0" {{$post->status=='0'?'checked':''}}>
                    <label for="status0">Hide</label>
                    <input type="radio" name="status" value="1" class="mx-2" id="status1" {{$post->status=='1'?'checked':''}}>
                    <label for="status1">Visible</label>
                </div>
                <button class="btn btn-primary text-white" type="submit">Submit</button>
            </form>
        </div>
    </div>
@endsection
