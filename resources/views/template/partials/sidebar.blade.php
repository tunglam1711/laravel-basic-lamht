<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{route('dashboard')}}">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('post.index')}}">
                <i class="mdi mdi-book-open-variant menu-icon"></i>
                <span class="menu-title">Post</span>
            </a>
        </li>
    </ul>
</nav>
