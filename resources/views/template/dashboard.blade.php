<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@yield('title')

<!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('theme/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/vendors/base/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{asset('theme/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('theme/css/style.css')}}">
{{--    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">--}}
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('theme/images/favicon.png')}}"/>

</head>

<body>
<!-- partial:partials/_navbar.html -->
@include('template.partials.navbar')
<!-- partial -->
<div class="container-fluid page-body-wrapper">
    <!-- partial:partials/_sidebar.html -->
@include('template.partials.sidebar')
<!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
    @include('template.partials.footer')
    <!-- partial -->
    </div>
    <!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
<!-- container-scroller -->

<!-- plugins:js -->
<script src="{{asset('theme/vendors/base/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="{{asset('theme/vendors/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('theme/vendors/datatables.net/jquery.dataTables.js')}}"></script>
<script src="{{asset('theme/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{asset('theme/js/off-canvas.js')}}"></script>
<script src="{{asset('theme/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('theme/js/template.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('theme/js/dashboard.js')}}"></script>
<script src="{{asset('theme/js/data-table.js')}}"></script>
<script src="{{asset('theme/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('theme/js/dataTables.bootstrap4.js')}}"></script>
<!-- End custom js for this page-->

<script src="{{asset('theme/js/jquery.cookie.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
</body>

</html>
