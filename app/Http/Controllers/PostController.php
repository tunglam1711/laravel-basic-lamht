<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\PostRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     * @return mixed
     */
    public function index()
    {
        $posts = $this->post->paginate(7);
        return view('template.post.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     * @return mixed
     */
    public function create()
    {
        return view('template.post.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param PostRequest $request
     * @return mixed
     */
    public function store(PostRequest $request)
    {
        $dataStore = $request->all();
        $this->post->create($dataStore);
        return redirect()->route('post.index')->with(['message'=>'Create New Post Successfully']);
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $post = $this->post->findOrFail($id);
        return view('template.post.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     * @param PostRequest $request
     * @param $id
     * @return mixed
     */
    public function update(PostRequest $request, $id)
    {
        $post = $this->post->findOrFail($id);
        $dataUpdate = $request->all();
        $post->update($dataUpdate);
        return redirect()->route('post.index')->with(['message'=>'Update Post Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $post = $this->post->findOrFail($id);
        $post->delete();
        return redirect()->route('post.index')->with(['message'=>'Delete Post Successfully']);
    }
}
